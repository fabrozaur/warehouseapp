package sample;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;

public class Item {

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getCostSum() {
        return costSum;
    }

    public void setCostSum(Float costSum) {
        this.costSum = costSum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getSoldPrice() {
        return soldPrice;
    }

    public void setSoldPrice(Float soldPrice) {
        this.soldPrice = soldPrice;
    }

    public Integer getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(Integer soldAmount) {
        this.soldAmount = soldAmount;
    }

    public Float getProfit() {
        return profit;
    }

    public void setProfit(Float profit) {
        this.profit = profit;
    }


    private Integer id;
    private String name;
    private Integer quantity;
    private Float price;
    private Float costSum;
    private Float soldPrice;
    private Integer soldAmount;
    private Float profit;


    public Item(Integer id, String name, Integer quantity, Float price, Float soldPrice, Integer soldAmount) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.costSum = quantity*price;
        this.soldPrice = soldPrice;
        this.soldAmount = soldAmount;
        if(this.costSum > 0 && soldAmount > 0){
            this.profit = this.soldPrice * this.soldAmount - costSum;
        }
    }
}
