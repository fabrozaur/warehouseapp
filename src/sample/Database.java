package sample;

import java.sql.*;

public class Database {

    public Connection connection;
    private Boolean hasData = false;


    public ResultSet displayData() throws ClassNotFoundException, SQLException {
        if(connection == null || connection.isClosed()){
            getConnection();
        }
        Statement state = connection.createStatement();
        return state.executeQuery("SELECT Store.id, Store.Name, Store.Quantity, Store.Price, Store.Sell_amount, SELL_PRICE FROM STORE;");
    }

    public ResultSet displayByID(Integer arg) throws ClassNotFoundException, SQLException {
        if(connection == null || connection.isClosed()){
            getConnection();
        }
        PreparedStatement prep = connection.prepareStatement("SELECT Store.id, Store.Name, Store.Quantity, Store.Price, Store.Sell_amount, SELL_PRICE FROM STORE WHERE ID = ?;");
        prep.setInt(1,arg);
        prep.execute();
        return prep.getResultSet();
    }

    private void getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("JDBC:sqlite:Store.db");
        initialize();
    }

    private void initialize() throws SQLException {
        if (!hasData){
            hasData = true;

            Statement state = connection.createStatement();
            ResultSet res = state.executeQuery("SELECT name FROM sqlite_master WHERE TYPE = 'table' AND name = 'Store';");
            if(!res.next()){
                state.execute("CREATE TABLE Store (ID INTEGER PRIMARY KEY, NAME varchar(60), QUANTITY INTEGER, PRICE FLOAT, SELL_AMOUNT INTEGER, SELL_PRICE FLOAT);");
            }
        }
    }

    public void delete_item(Integer arg) throws SQLException, ClassNotFoundException {
        if (connection == null || connection.isClosed())
        {
            getConnection();
        }
        PreparedStatement prep = connection.prepareStatement("DELETE FROM Store WHERE ID = ?;");
        prep.setInt(1,arg);
        prep.execute();
        connection.close();
    }

    public void addSellData(Integer sellAmount, Float sellPrice, Integer arg) throws SQLException, ClassNotFoundException {
        if (connection == null || connection.isClosed())
        {
            getConnection();
        }
        PreparedStatement prep = connection.prepareStatement("UPDATE Store SET SELL_AMOUNT = ?, SELL_PRICE = ? WHERE ID = ?;");
        prep.setInt(1,sellAmount);
        prep.setFloat(2,sellPrice);
        prep.setInt(3,arg);
        prep.execute();
        connection.close();
    }

    public void addExistingData(Integer quantity, Float price, Integer arg) throws SQLException, ClassNotFoundException {
        if (connection == null || connection.isClosed())
        {
            getConnection();
        }
        PreparedStatement prep = connection.prepareStatement("UPDATE Store SET QUANTITY = ?, PRICE = ? WHERE ID = ?;");
        prep.setInt(1,quantity);
        prep.setFloat(2,price);
        prep.setInt(3,arg);
        prep.execute();
        connection.close();
    }

    public void add_item(String x, Integer y, Float z) throws SQLException, ClassNotFoundException {
        if (connection == null || connection.isClosed())
        {
            getConnection();
        }
        PreparedStatement prep = connection.prepareStatement("INSERT INTO Store values(NULL,?,?,?,?,?);");
        prep.setString(1, x);
        prep.setInt(2, y);
        prep.setFloat(3, z);
        prep.execute();
        connection.close();
    }

}
