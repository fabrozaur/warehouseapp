package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.sql.SQLException;

public class AddItemController {

    private MainController parent;
    private ToggleGroup group;

    public static Float getCurrency() {
        return currency;
    }

    public static void setCurrency(Float currency) {
        AddItemController.currency = currency;
    }

    private static Float currency;

    @FXML
    private TextField NameField;

    @FXML
    private TextField PriceField;

    @FXML
    private TextField NumberField;

    @FXML
    private Button Accept;

    @FXML
    private RadioButton PLNButton;

    @FXML
    private RadioButton USDButton;

    @FXML
    void Exit(ActionEvent event) {
        Stage stage = (Stage) Accept.getScene().getWindow();
        stage.close();
    }

    public AddItemController() throws SQLException, ClassNotFoundException {
    }

    public void init(MainController ob)
    {
        parent = ob;
    }

    @FXML
    private void DataAccepted(ActionEvent event) throws SQLException, ClassNotFoundException {
        if(NameField.getLength() != 0 && PriceField.getLength() != 0 && NumberField.getLength() != 0) {
            try {
                if(USDButton.isSelected())
                    parent.data.add_item(NameField.getText(), Integer.parseInt(NumberField.getText()), Float.parseFloat(PriceField.getText()) * currency);
                else
                    parent.data.add_item(NameField.getText(), Integer.parseInt(NumberField.getText()), Float.parseFloat(PriceField.getText()));
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Błąd wprowadzenia");
                alert.setContentText("Wprowadzono zły typ danych");
                alert.showAndWait();
                return;
                }

            parent.update_store();
        }
        Stage stage = (Stage) Accept.getScene().getWindow();
        stage.close();
    }

    public void initialize(){
        group = new ToggleGroup();
        USDButton.setToggleGroup(group);
        PLNButton.setToggleGroup(group);
    }
}
