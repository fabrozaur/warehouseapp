package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Item;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SellStageController {

    MainController parent;

    @FXML
    private TextField priceField;

    @FXML
    private TextField quantityField;

    @FXML
    void Exit(ActionEvent event) {
        Stage stage = (Stage) priceField.getScene().getWindow();
        stage.close();
    }

    @FXML
    void Accept(ActionEvent event) throws SQLException, ClassNotFoundException {
        if (priceField.getLength() != 0 && quantityField.getLength() != 0) {
            Item item = parent.itemTable.getSelectionModel().getSelectedItem();
            ResultSet res = parent.data.displayByID(item.getId());
            try {
                parent.data.addSellData(res.getInt("SELL_AMOUNT") + Integer.parseInt(quantityField.getText()), (Float.parseFloat(priceField.getText()) * Integer.parseInt(quantityField.getText()) + res.getInt("SELL_AMOUNT") * res.getFloat("SELL_PRICE")) / (Integer.parseInt(quantityField.getText()) + res.getInt("SELL_AMOUNT")), item.getId());
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Błąd wprowadzenia");
                alert.setContentText("Wprowadzono zły typ danych");
                alert.showAndWait();
                return;
            }
            parent.update_store();
            Stage stage = (Stage) priceField.getScene().getWindow();
            stage.close();
        }
    }

    public void init(MainController ob) {
        parent = ob;
        if (parent.itemTable.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd zaznaczenia");
            alert.setContentText("Nie zaznaczono żadnych danych");
            alert.showAndWait();
            Stage stage = (Stage) priceField.getScene().getWindow();
            stage.close();
        }
    }

}
