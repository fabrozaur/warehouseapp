package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import sample.Item;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddExistingItemController {

    private MainController parent;
    private ToggleGroup group;

    @FXML
    private TextField priceField;

    @FXML
    private TextField quantityField;

    @FXML
    private RadioButton PLNRadio;

    @FXML
    private RadioButton USDRadio;

    @FXML
    void Accept(ActionEvent event) throws SQLException, ClassNotFoundException {
        if(!parent.itemTable.getSelectionModel().isEmpty()) {
            if(priceField.getLength() != 0 && quantityField.getLength() != 0 ) {
                Item item = parent.itemTable.getSelectionModel().getSelectedItem();
                ResultSet res = parent.data.displayByID(item.getId());
                try {
                    if (PLNRadio.isSelected())
                        parent.data.addExistingData(res.getInt("QUANTITY") + Integer.parseInt(quantityField.getText()), (Float.parseFloat(priceField.getText()) * Integer.parseInt(quantityField.getText()) + res.getInt("QUANTITY") * res.getFloat("PRICE")) / (Integer.parseInt(quantityField.getText()) + res.getInt("QUANTITY")), item.getId());
                    else
                        parent.data.addExistingData(res.getInt("QUANTITY") + Integer.parseInt(quantityField.getText()), (Float.parseFloat(priceField.getText()) * Integer.parseInt(quantityField.getText()) * AddItemController.getCurrency() + res.getInt("QUANTITY") * res.getFloat("PRICE")) / (Integer.parseInt(quantityField.getText()) + res.getInt("QUANTITY")), item.getId());
                    parent.update_store();
                }catch (NumberFormatException e){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Błąd wprowadzenia");
                    alert.setContentText("Wprowadzono zły typ danych");
                    alert.showAndWait();
                }
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd zaznaczenia");
            alert.setContentText("Nie zaznaczono żadnych danych");
            alert.showAndWait();
        }
        Stage stage = (Stage) priceField.getScene().getWindow();
        stage.close();
    }

    @FXML
    void Exit(ActionEvent event){
        Stage stage = (Stage) priceField.getScene().getWindow();
        stage.close();
    }

    public void init(MainController ob){
        parent = ob;
        PLNRadio.setToggleGroup(group);
        USDRadio.setToggleGroup(group);
        if (parent.itemTable.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd zaznaczenia");
            alert.setContentText("Nie zaznaczono żadnych danych");
            alert.showAndWait();

            Stage stage = (Stage) priceField.getScene().getWindow();
            stage.close();
        }
    }
}
