package controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import sample.Item;
import sample.Database;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;


public class MainController {

    public Database data;
    //Stages
    private Stage insertProduct, sellingProduct, addMoreItems;
    private ObservableList<Item> StoreObservableList = FXCollections.observableArrayList();
    Document doc;

    @FXML
    private AnchorPane mainpane;

    @FXML
    private Button AddButton;

    @FXML
    private Button deletebutton;

    @FXML
    private CheckBox checkName;

    @FXML
    private CheckBox checkQuantity;

    @FXML
    private CheckBox checkPrice;

    @FXML
    private CheckBox checkValue;

    @FXML
    private CheckBox checkProfit;

    @FXML
    private CheckBox checkSellPrice;

    @FXML
    private CheckBox checkSellAmount;

    @FXML
    protected TableView<Item> itemTable;

    @FXML
    private TableColumn<Item, String> itemTableName;

    @FXML
    private TableColumn<Item, Integer> itemTableQuantity;

    @FXML
    private TableColumn<Item, Float> itemTablePrice;

    @FXML
    private TableColumn<Item, Float> itemTableValueOfAll;

    @FXML
    private TableColumn<Item, Float> itemTableSoldPrice;

    @FXML
    private TableColumn<Item, Integer> itemTableSoldQuantity;

    @FXML
    private TableColumn<Item, Float> itemTableProfit;

    @FXML
    private Button addToSoldButton;

    @FXML
    private TextField searchField;

    @FXML
    void checkNameAction(ActionEvent event) {
        if (checkName.isSelected()) {
            itemTableName.setVisible(true);
        } else
            itemTableName.setVisible(false);
    }

    @FXML
    void checkPriceAction(ActionEvent event) {
        if (checkPrice.isSelected()) {
            itemTablePrice.setVisible(true);
        } else
            itemTablePrice.setVisible(false);
    }

    @FXML
    void checkQuantityAction(ActionEvent event) {
        if (checkQuantity.isSelected()) {
            itemTableQuantity.setVisible(true);
        } else
            itemTableQuantity.setVisible(false);
    }

    @FXML
    void checkSellAmountAction(ActionEvent event) {
        if (checkSellAmount.isSelected()) {
            itemTableSoldQuantity.setVisible(true);
        } else
            itemTableSoldQuantity.setVisible(false);
    }

    @FXML
    void checkSellPriceAction(ActionEvent event) {
        if (checkSellPrice.isSelected()) {
            itemTableSoldPrice.setVisible(true);
        } else
            itemTableSoldPrice.setVisible(false);
    }

    @FXML
    void checkValueAction(ActionEvent event) {
        if (checkValue.isSelected()) {
            itemTableValueOfAll.setVisible(true);
        } else
            itemTableValueOfAll.setVisible(false);
    }

    @FXML
    void checkProfitAction(ActionEvent event) {
        if (checkProfit.isSelected()) {
            itemTableProfit.setVisible(true);
        } else
            itemTableProfit.setVisible(false);
    }


    @FXML
    void addToSold(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/sellStage.fxml"));
        sellingProduct = new Stage();
        sellingProduct.setResizable(false);
        sellingProduct.initModality(Modality.WINDOW_MODAL);
        sellingProduct.initOwner(mainpane.getScene().getWindow());
        sellingProduct.setTitle("Dodaj do sprzedanych");
        sellingProduct.setScene(new Scene(loader.load()));
        SellStageController sellStageController = loader.getController();
        sellingProduct.show();
        sellStageController.init(this);
    }

    @FXML
    void deletionOnAction(ActionEvent event) throws SQLException, ClassNotFoundException {
        if (!itemTable.getSelectionModel().isEmpty()) {
            Item item = itemTable.getSelectionModel().getSelectedItem();
            data.delete_item(item.getId());
            update_store();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd zaznaczenia");
            alert.setContentText("Nie zaznaczono żadnych danych");
            alert.showAndWait();
        }
    }

    @FXML
    void addToExistingItem(ActionEvent event) throws IOException {
        addMoreItems = new Stage();
        addMoreItems.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/addExistingItem.fxml"));
        addMoreItems.initModality(Modality.WINDOW_MODAL);
        addMoreItems.initOwner(mainpane.getScene().getWindow());
        addMoreItems.setTitle("Dodaj już istniejący produkt");
        addMoreItems.setScene(new Scene(loader.load()));
        AddExistingItemController addExistingItemController = loader.getController();
        addMoreItems.show();
        addExistingItemController.init(this);
    }

    @FXML
    void add_item(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        insertProduct = new Stage();
        insertProduct.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/additem.fxml"));
        insertProduct.initModality(Modality.WINDOW_MODAL);
        insertProduct.initOwner(mainpane.getScene().getWindow());
        insertProduct.setTitle("Dodaj nowy przedmiot");
        insertProduct.setScene(new Scene(loader.load()));
        AddItemController addItemController = loader.getController();
        addItemController.init(this);
        insertProduct.show();
    }

    public MainController() throws SQLException, ClassNotFoundException, NullPointerException {
        data = new Database();
        try {
            doc = Jsoup.connect("http://www.waluty.pl/").get();
            Elements ele = doc.select("div.wrap-table:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3) > span:nth-child(1) > strong:nth-child(1)");
            AddItemController.setCurrency(Float.parseFloat(ele.text()));
        } catch (IOException e) {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Problem z połączeniem");
            dialog.setHeaderText("Nie udało się ustalić wartości waluty");
            dialog.setContentText("Proszę wpisać wartość dolara: ");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> AddItemController.setCurrency(Float.parseFloat(name)));
        }

    }

    public void update_store() throws SQLException, ClassNotFoundException {
        ResultSet res = data.displayData();
        StoreObservableList.clear();
        while (res.next()) {
            StoreObservableList.add(new Item(res.getInt("ID"), res.getString("NAME"), res.getInt("QUANTITY"), res.getFloat("PRICE"), res.getFloat("SELL_PRICE"), res.getInt("SELL_AMOUNT")));
        }
        data.connection.close();
    }

    @FXML
    public void initialize() throws SQLException, ClassNotFoundException {
        update_store();
        itemTable.setItems(StoreObservableList);
        itemTableName.setCellValueFactory(new PropertyValueFactory<>("name"));
        itemTableQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        itemTablePrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        itemTableValueOfAll.setCellValueFactory(new PropertyValueFactory<>("costSum"));
        itemTableSoldPrice.setCellValueFactory(new PropertyValueFactory<>("soldPrice"));
        itemTableSoldQuantity.setCellValueFactory(new PropertyValueFactory<>("soldAmount"));
        itemTableProfit.setCellValueFactory(new PropertyValueFactory<>("profit"));

        searchField.lengthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (searchField.getLength() != 0) {
                    try {
                        update_store();
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    ArrayList temp = new ArrayList();
                    for (Item item : StoreObservableList) {
                        if (item.getName().toLowerCase().startsWith(searchField.getText().toLowerCase())) {
                            temp.add(item);
                        }
                    }
                    StoreObservableList.clear();
                    StoreObservableList.setAll(temp);
                } else {
                    try {
                        update_store();
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}